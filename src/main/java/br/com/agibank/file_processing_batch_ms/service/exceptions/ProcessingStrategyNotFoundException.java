package br.com.agibank.file_processing_batch_ms.service.exceptions;

public class ProcessingStrategyNotFoundException extends UseCaseException {
    public ProcessingStrategyNotFoundException() {
        super("Processing strategy for provided code was not found");
    }
}
