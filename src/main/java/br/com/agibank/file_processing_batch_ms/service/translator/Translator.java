package br.com.agibank.file_processing_batch_ms.service.translator;

public interface Translator <A,B>{
    B translate(A a);
}
