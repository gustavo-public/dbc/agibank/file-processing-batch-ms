package br.com.agibank.file_processing_batch_ms.service.use_case;

import br.com.agibank.file_processing_batch_ms.domain.FileProcessData;
import br.com.agibank.file_processing_batch_ms.service.exceptions.RegisterSaleUseCaseException;
import br.com.agibank.file_processing_batch_ms.service.translator.StringArrayToSaleTranslator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Slf4j
@Service
@RequiredArgsConstructor
public class RegisterSaleUseCase implements UseCase<FileProcessData, FileProcessData> {

    private final StringArrayToSaleTranslator stringArrayToSaleTranslator;

    private final CalculateSaleAmountUseCase calculateSaleAmountUseCase;
    private final RegisterSalesManSaleUseCase registerSalesManSaleUseCase;
    private final CheckExpensiveSaleUseCase checkExpensiveSaleUseCase;

    @Override
    public Mono<FileProcessData> execute(FileProcessData fileProcessData) {

        return Mono.just(fileProcessData)
                .map(FileProcessData::getLineData)
                .map(stringArrayToSaleTranslator::translate)
                .doOnNext(s -> log.info("Registering sale - {}", s.getId()))
                .flatMap(calculateSaleAmountUseCase::execute)
                .doOnNext(fileProcessData::setCurrentSale)
                .flatMap(sale -> checkExpensiveSaleUseCase.execute(fileProcessData))
                .flatMap(registerSalesManSaleUseCase::execute)
                .onErrorMap(RegisterSaleUseCaseException::new);

    }
}
