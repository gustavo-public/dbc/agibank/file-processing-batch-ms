package br.com.agibank.file_processing_batch_ms.service.use_case;

import br.com.agibank.file_processing_batch_ms.service.exceptions.ReadFileUseCaseException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@Slf4j
@Service
public class ReadFileUseCase implements UseCase<Path, String> {

    @Override
    public Mono<String> execute(Path path) {
        return Mono.just(path)
                .doOnNext(p -> log.info("Reading date from file: {}",p))
                .map(p -> {
                    try {
                        return Files.readString(p);
                    } catch (IOException ex) {
                        throw new RuntimeException("Error during file reading",ex);
                    }
                })
                .doOnNext(text -> log.info("File content read successfully"))
                .onErrorMap(ReadFileUseCaseException::new);
    }
}
