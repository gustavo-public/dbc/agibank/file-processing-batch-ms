package br.com.agibank.file_processing_batch_ms.service.use_case;

import br.com.agibank.file_processing_batch_ms.domain.FileProcessData;
import br.com.agibank.file_processing_batch_ms.domain.Sale;
import br.com.agibank.file_processing_batch_ms.service.exceptions.CheckExpensiveSaleUseCaseException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;

import static java.util.Objects.isNull;

@Slf4j
@Service
public class CheckExpensiveSaleUseCase implements UseCase<FileProcessData,FileProcessData>{

    @Override
    public Mono<FileProcessData> execute(FileProcessData fileProcessData) {
        return Mono.just(fileProcessData)
                    .doOnNext(f -> {
                        log.info("Cheking sale more expensive");
                        Sale currentExpensiveSale = f.getExpensiveSale();
                        Sale currentSale = f.getCurrentSale();
                        if(isNull(currentExpensiveSale)){
                            f.setExpensiveSale(currentSale);
                        }else {
                            BigDecimal currentValue = currentSale.getSaleAmount();
                            BigDecimal currentExpensiveValue = currentExpensiveSale.getSaleAmount();
                            if(currentValue.compareTo(currentExpensiveValue) > 0){
                                f.setExpensiveSale(currentSale);
                            }
                        }
                    }).onErrorMap(CheckExpensiveSaleUseCaseException::new);
    }
}
