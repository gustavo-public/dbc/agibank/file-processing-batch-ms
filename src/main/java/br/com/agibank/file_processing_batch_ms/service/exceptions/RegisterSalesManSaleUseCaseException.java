package br.com.agibank.file_processing_batch_ms.service.exceptions;

public class RegisterSalesManSaleUseCaseException extends UseCaseException {
    public RegisterSalesManSaleUseCaseException(Throwable cause) {
        super("Error on registering sale for salesman",cause);
    }
}
