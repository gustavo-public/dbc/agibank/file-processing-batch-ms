package br.com.agibank.file_processing_batch_ms.service.exceptions;

public class CalculateSaleAmountUseCaseException extends UseCaseException {
    public CalculateSaleAmountUseCaseException(Throwable cause) {
        super("Error on calculate sale amount", cause);
    }
}
