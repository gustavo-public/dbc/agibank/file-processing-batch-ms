package br.com.agibank.file_processing_batch_ms.service.translator;

import br.com.agibank.file_processing_batch_ms.domain.SalesMan;
import br.com.agibank.file_processing_batch_ms.domain.enums.INDEXES.SALES_MAN;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class StringArrayToSalesManTranslator implements Translator<String[], SalesMan>{

    @Override
    public SalesMan translate(String[] data) {
        return SalesMan.builder()
                        .cpf(data[SALES_MAN.CPF.getIndex()])
                        .name(data[SALES_MAN.NAME.getIndex()])
                        .salary(new BigDecimal(data[SALES_MAN.SALARY.getIndex()]))
                        .build();
    }

}
