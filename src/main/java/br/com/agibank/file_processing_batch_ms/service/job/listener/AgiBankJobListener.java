package br.com.agibank.file_processing_batch_ms.service.job.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class AgiBankJobListener extends JobExecutionListenerSupport {

    @Override
    public void afterJob(JobExecution jobExecution) {
        log.info("After job - {}",jobExecution.getJobId());
        super.afterJob(jobExecution);
    }

    @Override
    public void beforeJob(JobExecution jobExecution) {
        log.info("Before job - {}",jobExecution.getJobId());
        super.beforeJob(jobExecution);
    }
}
