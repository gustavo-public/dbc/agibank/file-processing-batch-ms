package br.com.agibank.file_processing_batch_ms.service.use_case;

import br.com.agibank.file_processing_batch_ms.domain.FileProcessData;
import br.com.agibank.file_processing_batch_ms.service.exceptions.RegisterSalesManUseCaseException;
import br.com.agibank.file_processing_batch_ms.service.translator.StringArrayToSalesManTranslator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Slf4j
@Service
@RequiredArgsConstructor
public class RegisterSalesManUseCase implements UseCase<FileProcessData,FileProcessData>{

    private final StringArrayToSalesManTranslator stringArrayToSalesManTranslator;

    @Override
    public Mono<FileProcessData> execute(FileProcessData fileProcessData) {
        return Mono.just(fileProcessData)
                    .map(FileProcessData::getLineData)
                    .map(stringArrayToSalesManTranslator::translate)
                    .doOnNext(s -> log.info("Registering salesman: {}",s.getName()))
                    .doOnNext(salesMan -> fileProcessData.getSalesManList().add(salesMan))
                    .map(salesMan -> fileProcessData)
                    .onErrorMap(RegisterSalesManUseCaseException::new)
                    ;
    }
}
