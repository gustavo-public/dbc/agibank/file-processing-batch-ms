package br.com.agibank.file_processing_batch_ms.service.exceptions;

public class CalculateMoreExpensiveSaleIdUseCaseException extends UseCaseException {
    public CalculateMoreExpensiveSaleIdUseCaseException(Throwable cause) {
        super("Error on calculate more expensive sale id",cause);
    }
}
