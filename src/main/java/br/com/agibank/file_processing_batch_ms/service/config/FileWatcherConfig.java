package br.com.agibank.file_processing_batch_ms.service.config;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.devtools.filewatch.FileSystemWatcher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PreDestroy;
import java.io.File;

import static java.io.File.separator;
import static java.time.Duration.ofMillis;

@Slf4j
@Configuration
@RequiredArgsConstructor
public class FileWatcherConfig {

    @Value("${file_watcher.check.in.dir}")
    private String checkInDir;
    @Value("${file_watcher.pollInterval}")
    private Long pollInterval;
    @Value("${file_watcher.quietPeriod}")
    private Long quietPeriod;

    private final AgiBankFileChangeListener agiBankFileChangeListener;

    @Bean
    public FileSystemWatcher fileSystemWatcher() {

        final String HOME_DIR = System.getProperty("user.home");
        final String SOURCE_DIR = HOME_DIR.concat(separator).concat(checkInDir);

        log.info("Watching directory: {}",SOURCE_DIR);

        FileSystemWatcher fileSystemWatcher = new FileSystemWatcher(true, ofMillis(pollInterval), ofMillis(quietPeriod));
        fileSystemWatcher.addSourceDirectory(new File(SOURCE_DIR));
        fileSystemWatcher.addListener(agiBankFileChangeListener);
        fileSystemWatcher.start();

        return fileSystemWatcher;
    }

    @PreDestroy
    public void onDestroy() throws Exception {
        fileSystemWatcher().stop();
    }

}
