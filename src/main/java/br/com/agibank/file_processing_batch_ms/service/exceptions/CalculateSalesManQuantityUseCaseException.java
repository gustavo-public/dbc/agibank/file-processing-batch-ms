package br.com.agibank.file_processing_batch_ms.service.exceptions;

public class CalculateSalesManQuantityUseCaseException extends UseCaseException {
    public CalculateSalesManQuantityUseCaseException(Throwable cause) {
        super("Error on calculating salesmen quantity", cause);
    }
}
