package br.com.agibank.file_processing_batch_ms.service.use_case.report;

import br.com.agibank.file_processing_batch_ms.domain.FileProcessData;
import br.com.agibank.file_processing_batch_ms.domain.Sale;
import br.com.agibank.file_processing_batch_ms.service.exceptions.CalculateMoreExpensiveSaleIdUseCaseException;
import br.com.agibank.file_processing_batch_ms.service.use_case.UseCase;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.Objects;

import static java.util.Objects.nonNull;

@Slf4j
@Service
public class CalculateMoreExpensiveSaleIdUseCase implements UseCase<FileProcessData,FileProcessData> {

    @Override
    public Mono<FileProcessData> execute(FileProcessData fileProcessData) {
        return Mono.just(fileProcessData)
                    .doOnNext(f -> log.info("Calculating mor expensive sale id"))
                    .filter(f -> nonNull(f.getExpensiveSale()))
                    .map(FileProcessData::getExpensiveSale)
                    .map(Sale::getId)
                    .doOnNext(fileProcessData::setMoreExpensiveSaleId)
                    .map(id -> fileProcessData)
                    .switchIfEmpty(Mono.just(fileProcessData))
                    .onErrorMap(CalculateMoreExpensiveSaleIdUseCaseException::new);

    }
}
