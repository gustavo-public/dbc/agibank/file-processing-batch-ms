package br.com.agibank.file_processing_batch_ms.service.use_case;

import br.com.agibank.file_processing_batch_ms.domain.FileProcessData;
import br.com.agibank.file_processing_batch_ms.service.exceptions.RegisterClientUseCaseException;
import br.com.agibank.file_processing_batch_ms.service.translator.StringArrayToClientTranslator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Slf4j
@Service
@RequiredArgsConstructor
public class RegisterClientUseCase implements UseCase<FileProcessData,FileProcessData>{

    private final StringArrayToClientTranslator stringArrayToClientTranslator;

    @Override
    public Mono<FileProcessData> execute(FileProcessData fileProcessData) {
        return Mono.just(fileProcessData)
                    .map(FileProcessData::getLineData)
                    .map(stringArrayToClientTranslator::translate)
                    .map(client -> {
                        log.info("Registering client: {}", client.getName());
                        fileProcessData.getClientList().add(client);
                        return fileProcessData;
                    }).onErrorMap(RegisterClientUseCaseException::new);
    }

}
