package br.com.agibank.file_processing_batch_ms.service.exceptions;

public class CalculateWorstSalesManUseCaseException extends UseCaseException {
    public CalculateWorstSalesManUseCaseException(Throwable cause) {
        super("Error on calculate worst salesman", cause);
    }
}
