package br.com.agibank.file_processing_batch_ms.service.translator;

import br.com.agibank.file_processing_batch_ms.domain.SaleItem;
import br.com.agibank.file_processing_batch_ms.domain.enums.INDEXES;
import br.com.agibank.file_processing_batch_ms.domain.enums.INDEXES.SALE_ITEM;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class StringArrayToSaleItemTranslator implements Translator<String[], SaleItem> {

    @Override
    public SaleItem translate(String[] data) {
        return SaleItem.builder()
                    .id(data[SALE_ITEM.ID.getIndex()])
                    .price(new BigDecimal(data[SALE_ITEM.PRICE.getIndex()]))
                    .quantity(new BigDecimal(data[SALE_ITEM.QUANTITY.getIndex()]))
                    .build();
    }
}
