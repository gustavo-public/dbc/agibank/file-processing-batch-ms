package br.com.agibank.file_processing_batch_ms.service.job.processor;

import br.com.agibank.file_processing_batch_ms.domain.FileProcessData;
import br.com.agibank.file_processing_batch_ms.domain.FileProcessReport;
import br.com.agibank.file_processing_batch_ms.domain.enums.INDEXES;
import br.com.agibank.file_processing_batch_ms.service.use_case.SelectProcessingStrategyUseCase;
import br.com.agibank.file_processing_batch_ms.service.use_case.report.GenerateReportDataUseCase;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.Arrays;

@Slf4j
@Component
@RequiredArgsConstructor
public class AgiBankFileProcessor implements ItemProcessor<FileProcessData, FileProcessReport> {

    @Value("${codes.sales_man_code}")
    private String salesManCode;
    @Value("${codes.sales_code}")
    private String salesCode;
    @Value("${codes.client_code}")
    private String clientCode;
    @Value("${file_watcher.line.item.separator}")
    private String separator;

    private final SelectProcessingStrategyUseCase selectProcessingStrategyUseCase;
    private final GenerateReportDataUseCase generateReportDataUseCase;

    @Override
    public FileProcessReport process(FileProcessData fileProcessData) throws Exception {
        log.info("Starting data processing");
        return Mono.just(fileProcessData)
                .map(f -> {
                    f.getFileContent()
                            .lines()
                            .map(l -> l.split(separator))
                            .forEach(data -> {
                                log.info("Processing line: {}", Arrays.toString(data));
                                String code = data[INDEXES.CODE.getIndex()];
                                fileProcessData.setLineData(data);
                                selectProcessingStrategyUseCase.execute(code)
                                        .flatMap(func -> func.apply(fileProcessData))
                                        .block();
                            });
                    return f;
                })
                .flatMap(generateReportDataUseCase::execute)
                .blockOptional()
                .orElse(null);
    }

}
