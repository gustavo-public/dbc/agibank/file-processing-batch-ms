package br.com.agibank.file_processing_batch_ms.service.translator;

import br.com.agibank.file_processing_batch_ms.domain.Client;
import br.com.agibank.file_processing_batch_ms.domain.enums.INDEXES;
import org.springframework.stereotype.Component;

@Component
public class StringArrayToClientTranslator implements Translator<String[], Client> {

    @Override
    public Client translate(String[] data) {
        return Client.builder()
                    .cnpj(data[INDEXES.CLIENT.CPNJ.getIndex()])
                    .name(data[INDEXES.CLIENT.NAME.getIndex()])
                    .businessArea(data[INDEXES.CLIENT.BUSINESS_AREA.getIndex()])
                    .build();
    }
}
