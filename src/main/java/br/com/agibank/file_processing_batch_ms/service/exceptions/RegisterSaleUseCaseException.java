package br.com.agibank.file_processing_batch_ms.service.exceptions;

public class RegisterSaleUseCaseException extends UseCaseException {
    public RegisterSaleUseCaseException(Throwable cause) {
        super("Error on registering sale", cause);
    }
}
