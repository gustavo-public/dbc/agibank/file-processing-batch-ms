package br.com.agibank.file_processing_batch_ms.service.exceptions;

public class ProcessingStrategyCodeRequiredException extends UseCaseException {

    public ProcessingStrategyCodeRequiredException() {
        super("Strategy code must be provided and be valid");
    }
}
