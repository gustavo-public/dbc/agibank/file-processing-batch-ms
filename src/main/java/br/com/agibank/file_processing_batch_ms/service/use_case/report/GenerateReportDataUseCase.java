package br.com.agibank.file_processing_batch_ms.service.use_case.report;

import br.com.agibank.file_processing_batch_ms.domain.FileProcessData;
import br.com.agibank.file_processing_batch_ms.domain.FileProcessReport;
import br.com.agibank.file_processing_batch_ms.service.use_case.UseCase;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Slf4j
@Service
@RequiredArgsConstructor
public class GenerateReportDataUseCase implements UseCase<FileProcessData, FileProcessReport> {

    private final CalculateClientQuantityUseCase calculateClientQuantityUseCase;
    private final CalculateSalesManQuantityUseCase calculateSalesManQuantityUseCase;
    private final CalculateWorstSalesManUseCase calculateWorstSalesManUseCase;
    private final CalculateMoreExpensiveSaleIdUseCase calculateMoreExpensiveSaleIdUseCase;

    //TODO - mapstruct nao esta gerando a implementação corretamente corrigir assim que concluir o projeto
//    private final FileProcessDataToFileProcessReportDataTransfer fileProcessDataToFileProcessReportDataTransfer;

    @Override
    public Mono<FileProcessReport> execute(FileProcessData fileProcessData) {

        return Mono.just(fileProcessData)
                .flatMap(calculateClientQuantityUseCase::execute)
                .flatMap(calculateSalesManQuantityUseCase::execute)
                .flatMap(calculateWorstSalesManUseCase::execute)
                .flatMap(calculateMoreExpensiveSaleIdUseCase::execute)
                .map(processData -> FileProcessReport.builder()
                        .startTime(processData.getStartTime())
                        .fileName(processData.getFileName())

                        .worstSalesManName(processData.getWorstSalesManName())
                        .salesManQuantity(processData.getSalesManQuantity())
                        .clientQuantity(processData.getClientQuantity())
                        .moreExpensiveSaleId(processData.getMoreExpensiveSaleId())

                        .build());
    }
}
