package br.com.agibank.file_processing_batch_ms.service.job.writer;

import br.com.agibank.file_processing_batch_ms.domain.FileProcessReport;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.convert.DurationFormat;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static java.io.File.separator;
import static java.util.Objects.isNull;

@Slf4j
@Component
@RequiredArgsConstructor
public class AgiBankFileWriter implements ItemWriter<FileProcessReport> {


    @Value("${file_watcher.check.out.dir}")
    private String checkOutDir;

    @Value("${file_watcher.check.out.extension}")
    private String checkOutExtension;
    @Value("${file_watcher.check.in.extension}")
    private String checkInExtension;

    @Override
    public void write(List<? extends FileProcessReport> items) throws Exception {
        log.info("Starting write processing");

        final String HOME_PATH = System.getProperty("user.home");
        final String absoluteCheckOutDir = HOME_PATH.concat(separator).concat(checkOutDir);

        items.forEach(fpr -> {
            String checkOutFileContent = this.buildReportTemplate(fpr);
            String checkOutFileName = renameFile(fpr.getFileName());

            Path checkOutPath = Path.of(absoluteCheckOutDir, checkOutFileName);
            log.info("Writing file: {}", checkOutPath);
            try {
                Files.write(checkOutPath, checkOutFileContent.getBytes());
                String duration = this.getStringDurationFrom(fpr.getStartTime());
                log.info("Processing duration: {}", duration);
            } catch (IOException ex) {
                log.info("Error on write file: {} | Cause: {}", checkOutPath, ex.getMessage());
            }
        });
        ;
    }

    private String buildReportTemplate(FileProcessReport reportData) {

        final String REPORT_TEMPLATE = """
                Quantidade de clientes no arquivo de entrada: %s
                Quantidade de vendedor no arquivo de entrada: %s
                ID da venda mais cara: %s
                O pior vendedor: %s
                """;

        return String.format(REPORT_TEMPLATE, reportData.getClientQuantity()
                , reportData.getSalesManQuantity()
                , reportData.getMoreExpensiveSaleId()
                , reportData.getWorstSalesManName());

    }

    private String getStringDurationFrom(LocalTime start) {
        final String PATERN = "HH:mm:ss.SSS";
        Duration duration = Duration.between(start, LocalTime.now());
        return LocalTime.MIDNIGHT.plus(duration).format(DateTimeFormatter.ofPattern(PATERN));
    }

    private String renameFile(String currentFileName) {

        return currentFileName.replaceAll(checkInExtension, checkOutExtension);

    }


}
