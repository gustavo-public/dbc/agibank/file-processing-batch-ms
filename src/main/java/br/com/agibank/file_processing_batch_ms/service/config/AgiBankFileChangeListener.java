package br.com.agibank.file_processing_batch_ms.service.config;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.devtools.filewatch.ChangedFile;
import org.springframework.boot.devtools.filewatch.ChangedFiles;
import org.springframework.boot.devtools.filewatch.FileChangeListener;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Date;
import java.util.Set;
import java.util.UUID;

import static org.springframework.boot.devtools.filewatch.ChangedFile.Type.ADD;

@Slf4j
@Component
@RequiredArgsConstructor
public class AgiBankFileChangeListener implements FileChangeListener {

    @Value("${file_watcher.check.in.extension}")
    private String checkInExtension;

    private final Job agiBankJob;

    private final JobLauncher agiBankJobLauncher;

    @Override
    public void onChange(Set<ChangedFiles> changeSet) {

        changeSet.stream()
                .peek(f -> log.info("File change detected"))
                .map(ChangedFiles::getFiles)
                .flatMap(f -> f.stream().filter(c -> ADD.equals(c.getType())).map(ChangedFile::getFile))
                .filter(f -> !isLocked(f.toPath()))
                .filter(f -> f.getName().endsWith(checkInExtension))
                .peek(f -> log.info("File adding detected, starting process."))
                .map(File::getAbsoluteFile)
                .map(this::buildJobParameters)
                .forEach(this::startJob);

    }

    private void startJob(JobParameters params){
        try{
            log.error("Starting job");
            agiBankJobLauncher.run(agiBankJob, params);
        }catch (Exception exception){
            log.error("Error on job processing file - {}",exception.getMessage(),exception);
        }
    }

    private JobParameters buildJobParameters(File absoluteFile){
        return new JobParametersBuilder()
                .addDate("execution_date", new Date())
                .addString("absolute_path", absoluteFile.getAbsolutePath())
                .toJobParameters();
    }

    private boolean isLocked(Path path) {
        try (FileChannel ch = FileChannel.open(path, StandardOpenOption.WRITE); FileLock lock = ch.tryLock()) {
            return lock == null;
        } catch (IOException e) {
            return true;
        }
    }

}
