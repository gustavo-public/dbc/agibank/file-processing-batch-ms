package br.com.agibank.file_processing_batch_ms.service.use_case;

import org.reactivestreams.Publisher;

public interface UseCase <A,B>{
    Publisher<B>execute(A a);
}
