package br.com.agibank.file_processing_batch_ms.service.exceptions;

public class ReadFileUseCaseException extends UseCaseException{

    public ReadFileUseCaseException(Throwable cause) {
        super("Error on read file", cause);
    }
}
