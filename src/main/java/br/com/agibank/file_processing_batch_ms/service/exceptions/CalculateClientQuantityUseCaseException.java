package br.com.agibank.file_processing_batch_ms.service.exceptions;

public class CalculateClientQuantityUseCaseException extends UseCaseException {
    public CalculateClientQuantityUseCaseException(Throwable cause) {
        super("Error on calculate client quantity", cause);
    }
}
