package br.com.agibank.file_processing_batch_ms.service.exceptions;

public class UseCaseException extends RuntimeException{

    protected UseCaseException(String message){
        super(message);
    }

    protected UseCaseException(String message,Throwable cause){
        super(message,cause);
    }
}
