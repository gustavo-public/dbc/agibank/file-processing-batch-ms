package br.com.agibank.file_processing_batch_ms.service.use_case.report;

import br.com.agibank.file_processing_batch_ms.domain.FileProcessData;
import br.com.agibank.file_processing_batch_ms.service.exceptions.CalculateClientQuantityUseCaseException;
import br.com.agibank.file_processing_batch_ms.service.use_case.UseCase;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Objects;

import static java.util.Objects.nonNull;

@Slf4j
@Service
public class CalculateClientQuantityUseCase implements UseCase<FileProcessData, FileProcessData> {

    @Override
    public Mono<FileProcessData> execute(FileProcessData fileProcessData) {
        return Mono.just(fileProcessData)
                .doOnNext(f -> log.info("Calculating client quantity"))
                .filter(f -> nonNull(f.getClientList()))
                .map(FileProcessData::getClientList)
                .map(List::size)
                .switchIfEmpty(Mono.just(0))
                .doOnNext(fileProcessData::setClientQuantity)
                .map(size -> fileProcessData)
                .onErrorMap(CalculateClientQuantityUseCaseException::new);
    }
}
