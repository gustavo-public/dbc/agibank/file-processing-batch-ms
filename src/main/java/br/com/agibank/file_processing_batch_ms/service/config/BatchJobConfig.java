package br.com.agibank.file_processing_batch_ms.service.config;

import br.com.agibank.file_processing_batch_ms.domain.FileProcessData;
import br.com.agibank.file_processing_batch_ms.domain.FileProcessReport;
import br.com.agibank.file_processing_batch_ms.service.job.listener.AgiBankJobListener;
import br.com.agibank.file_processing_batch_ms.service.job.processor.AgiBankFileProcessor;
import br.com.agibank.file_processing_batch_ms.service.job.reader.AgiBankFileReader;
import br.com.agibank.file_processing_batch_ms.service.job.writer.AgiBankFileWriter;
import br.com.agibank.file_processing_batch_ms.service.step.listener.AgiBankStepListener;
import lombok.RequiredArgsConstructor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SimpleAsyncTaskExecutor;

@Configuration
@RequiredArgsConstructor
public class BatchJobConfig {

    private final JobBuilderFactory jobBuilderFactory;
    private final StepBuilderFactory stepBuilderFactory;

    private final AgiBankJobListener agiBankJobListener;
    private final AgiBankStepListener agiBankStepListener;

    private final AgiBankFileReader agiBankFileReader;
    private final AgiBankFileWriter agiBankFileWriter;
    private final AgiBankFileProcessor agiBankFileProcessor;

    @Bean
    public JobLauncher agiBankJobLauncher(final JobRepository jobRepository) throws Exception {

        final SimpleJobLauncher simpleJobLauncher = new SimpleJobLauncher();
        simpleJobLauncher.setJobRepository(jobRepository);
        simpleJobLauncher.setTaskExecutor(new SimpleAsyncTaskExecutor());

        return simpleJobLauncher;
    }

    @Bean
    public Job agiBankJob() throws Exception {
        final String JOB_KEY = "agiBankJob";
        return jobBuilderFactory.get(JOB_KEY)
                .incrementer(new RunIdIncrementer())
                .listener(agiBankJobListener)
                .flow(fileProcessingStep())
                .end()
                .build();
    }


    @Bean
    public Step fileProcessingStep() {
        final int CHUNK_SIZE = 1000;
        final String STEP_KEY = "agiBankJobStep";
        return stepBuilderFactory.get(STEP_KEY)
                .<FileProcessData, FileProcessReport>chunk(CHUNK_SIZE)
                .reader(agiBankFileReader)
                .processor(agiBankFileProcessor)
                .writer(agiBankFileWriter)
                .listener(agiBankStepListener)
                .build();
    }

}
