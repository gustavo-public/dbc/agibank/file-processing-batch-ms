package br.com.agibank.file_processing_batch_ms.service.job.reader;

import br.com.agibank.file_processing_batch_ms.domain.FileProcessData;
import br.com.agibank.file_processing_batch_ms.service.use_case.ReadFileUseCase;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.nio.file.Path;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Optional;

@Slf4j
@StepScope
@Component
@RequiredArgsConstructor
public class AgiBankFileReader implements ItemReader<FileProcessData> {

    private final ReadFileUseCase readFileUseCase;

    @Value("#{jobParameters['id']}")
    private String id;
    @Value("#{jobParameters['absolute_path']}")
    private String fileAbsolutePath;

    private String currentFile;

    @Override
    public FileProcessData read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {

        return Optional.ofNullable(fileAbsolutePath)
                .map(Mono::just)
                .orElseGet(null)
                .filter(path -> !path.equals(currentFile))
                .doOnNext(path -> currentFile = path)
                .doOnNext(path -> log.info("Starting file reading - {}", path))
                .map(Path::of)
                .doOnNext(path -> log.info("Received file for reading - {}", path.getFileName().toString()))
                .flatMap(path -> readFileUseCase.execute(path)
                        .map(text -> FileProcessData
                                .builder()
                                .startTime(LocalTime.now())
                                .fileName(path.getFileName().toString())
                                .fileContent(text)
                                .salesManSaleAmountMap(new HashMap<>())
                                .clientList(new LinkedList<>())
                                .salesManList(new LinkedList<>())
                                .build())
                )
                .blockOptional()
                .orElse(null);

    }
}
