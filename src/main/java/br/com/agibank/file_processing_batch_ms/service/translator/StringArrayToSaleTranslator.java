package br.com.agibank.file_processing_batch_ms.service.translator;

import br.com.agibank.file_processing_batch_ms.domain.Sale;
import br.com.agibank.file_processing_batch_ms.domain.enums.INDEXES;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
@RequiredArgsConstructor
public class StringArrayToSaleTranslator implements Translator<String[], Sale> {

    private final StringToLinkedListSaleItemTranslator stringToLinkedListSaleItemTranslator;

    @Override
    public Sale translate(String[] data) {
        return Sale.builder()
                    .id(data[INDEXES.SALE.ID.getIndex()])
                    .saleAmount(new BigDecimal("0"))
                    .salesManName(data[INDEXES.SALE.SALES_MAN.getIndex()])
                    .items(stringToLinkedListSaleItemTranslator.translate(data[INDEXES.SALE.ITEMS.getIndex()]))
                    .build();
    }
}
