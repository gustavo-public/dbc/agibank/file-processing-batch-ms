package br.com.agibank.file_processing_batch_ms.service.exceptions;

public class RegisterClientUseCaseException extends UseCaseException {
    public RegisterClientUseCaseException(Throwable cause) {
        super("Error on registering client", cause);
    }
}
