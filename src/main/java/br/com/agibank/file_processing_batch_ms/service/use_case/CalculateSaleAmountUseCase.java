package br.com.agibank.file_processing_batch_ms.service.use_case;

import br.com.agibank.file_processing_batch_ms.domain.Sale;
import br.com.agibank.file_processing_batch_ms.service.exceptions.CalculateSaleAmountUseCaseException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static java.math.RoundingMode.HALF_EVEN;

@Slf4j
@Service
public class CalculateSaleAmountUseCase implements UseCase<Sale, Sale> {

    @Override
    public Mono<Sale> execute(Sale sale) {
        return Mono.just(sale)
                .doOnNext(s -> log.info("Calculating sale amount"))
                .map(Sale::getItems)
                .map(saleItems -> saleItems.stream()
                        .map(i -> i.getPrice()
                                .multiply(i.getQuantity())
                                .setScale(2, HALF_EVEN))
                        .reduce(new BigDecimal("0"), BigDecimal::add))
                .doOnNext(sale::setSaleAmount)
                .map(val -> sale)
                .onErrorMap(CalculateSaleAmountUseCaseException::new);
    }

}
