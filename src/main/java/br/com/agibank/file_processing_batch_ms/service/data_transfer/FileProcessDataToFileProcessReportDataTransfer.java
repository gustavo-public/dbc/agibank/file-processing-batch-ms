package br.com.agibank.file_processing_batch_ms.service.data_transfer;

import br.com.agibank.file_processing_batch_ms.domain.FileProcessData;
import br.com.agibank.file_processing_batch_ms.domain.FileProcessReport;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface FileProcessDataToFileProcessReportDataTransfer extends DataTransfer<FileProcessData, FileProcessReport>{

    @Override
    FileProcessReport transfer(FileProcessData fileProcessData,@MappingTarget FileProcessReport report);
}
