package br.com.agibank.file_processing_batch_ms.service.use_case.report;

import br.com.agibank.file_processing_batch_ms.domain.FileProcessData;
import br.com.agibank.file_processing_batch_ms.service.exceptions.CalculateWorstSalesManUseCaseException;
import br.com.agibank.file_processing_batch_ms.service.use_case.UseCase;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.Comparator;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

@Slf4j
@Service
public class CalculateWorstSalesManUseCase implements UseCase<FileProcessData,FileProcessData> {

    @Override
    public Mono<FileProcessData> execute(FileProcessData fileProcessData) {
        return Mono.just(fileProcessData)
                .map(FileProcessData::getSalesManSaleAmountMap)
                .map(Map::entrySet)
                .map(set -> set.stream().sorted(Map.Entry.comparingByValue()))
                .map(Stream::findFirst)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(Map.Entry::getKey)
                .doOnNext(fileProcessData::setWorstSalesManName)
                .map(name -> fileProcessData)
                .onErrorMap(CalculateWorstSalesManUseCaseException::new)
        ;

    }
}
