package br.com.agibank.file_processing_batch_ms.service.use_case;

import br.com.agibank.file_processing_batch_ms.domain.FileProcessData;
import br.com.agibank.file_processing_batch_ms.service.exceptions.ProcessingStrategyCodeRequiredException;
import br.com.agibank.file_processing_batch_ms.service.exceptions.ProcessingStrategyNotFoundException;
import br.com.agibank.file_processing_batch_ms.service.exceptions.SelectProcessingStrategyUseCaseException;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import static java.util.Optional.ofNullable;

@Slf4j
@Service
@RequiredArgsConstructor
public class SelectProcessingStrategyUseCase implements UseCase<String, Function<FileProcessData,Mono<FileProcessData>>> {

    private final RegisterSalesManUseCase registerSalesManUseCase;
    private final RegisterSaleUseCase registerSaleUseCase;
    private final RegisterClientUseCase registerClientUseCase;

    @Value("${codes.sales_man_code}")
    private String salesManCode;
    @Value("${codes.sales_code}")
    private String saleCode;
    @Value("${codes.client_code}")
    private String clientCode;

    private final Map<String, Function<FileProcessData,Mono<FileProcessData>>> useCaseMap = new HashMap<>();

    @Override
    public Mono<Function<FileProcessData, Mono<FileProcessData>>> execute(String code) {

        useCaseMap.put(salesManCode, registerSalesManUseCase::execute);
        useCaseMap.put(saleCode, registerSaleUseCase::execute);
        useCaseMap.put(clientCode, registerClientUseCase::execute);

        return Mono.just(ofNullable(code))
                .flatMap(opt -> opt.filter(StringUtils::hasText)
                        .map(Mono::just)
                        .orElseThrow(ProcessingStrategyCodeRequiredException::new)
                )
                .doOnNext(c -> log.info("Selecting processing strategy for code: {}", c))
                .map(c -> ofNullable(useCaseMap.get(c))
                        .orElseThrow(ProcessingStrategyNotFoundException::new))
                .onErrorMap(SelectProcessingStrategyUseCaseException::new);

    }

}
