package br.com.agibank.file_processing_batch_ms.service.exceptions;

public class RegisterSalesManUseCaseException extends UseCaseException {
    public RegisterSalesManUseCaseException(Throwable cause) {
        super("Error on registering salesman", cause);
    }
}
