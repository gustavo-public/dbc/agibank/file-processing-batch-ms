package br.com.agibank.file_processing_batch_ms.service.use_case;

import br.com.agibank.file_processing_batch_ms.domain.FileProcessData;
import br.com.agibank.file_processing_batch_ms.service.exceptions.RegisterSalesManSaleUseCaseException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.util.Map;

import static java.util.Optional.ofNullable;

@Slf4j
@Service
public class RegisterSalesManSaleUseCase implements UseCase<FileProcessData,FileProcessData>{

    @Override
    public Mono<FileProcessData> execute(FileProcessData fileProcessData) {
        return Mono.just(fileProcessData)
                    .map(FileProcessData::getCurrentSale)
                    .map(sale ->{

                        String name = sale.getSalesManName();

                        log.info("Registering sale for salesman - {}",name);

                        Map<String, BigDecimal>salesManMap = fileProcessData.getSalesManSaleAmountMap();

                        BigDecimal currentValue = ofNullable(salesManMap.get(name))
                                                            .orElse(new BigDecimal("0"));

                        BigDecimal updatedValue = currentValue.add(sale.getSaleAmount());

                        salesManMap.put(name,updatedValue);

                        return fileProcessData;
                    }).onErrorMap(RegisterSalesManSaleUseCaseException::new);
    }
}
