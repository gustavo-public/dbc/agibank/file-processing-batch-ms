package br.com.agibank.file_processing_batch_ms.service.translator;

import br.com.agibank.file_processing_batch_ms.domain.SaleItem;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Arrays.stream;

@Component
@RequiredArgsConstructor
public class StringToLinkedListSaleItemTranslator implements Translator<String, List<SaleItem>> {

    private final StringArrayToSaleItemTranslator stringArrayToSaleItemTranslator;

    @Value("${file_watcher.line.list.delimiter.start}")
    private String delimiterStart;
    @Value("${file_watcher.line.list.delimiter.end}")
    private String delimiterEnd;
    @Value("${file_watcher.line.list.item.separator}")
    private String itemSeparator;
    @Value("${file_watcher.line.list.item.data.separator}")
    private String itemDataSeparator;

    @Override
    public List<SaleItem> translate(String listDataString) {
        return stream(listDataString.replaceAll(delimiterStart, "")
                .replaceAll(delimiterEnd, "")
                .split(itemSeparator))
                .map(itemsArrayString -> itemsArrayString.split(itemDataSeparator))
                .map(stringArrayToSaleItemTranslator::translate)
                .collect(Collectors.toList())
                ;
    }
}
