package br.com.agibank.file_processing_batch_ms.service.exceptions;

public class CheckExpensiveSaleUseCaseException extends UseCaseException {
    public CheckExpensiveSaleUseCaseException(Throwable cause) {
        super("Error on checking more expensive sale", cause);
    }
}
