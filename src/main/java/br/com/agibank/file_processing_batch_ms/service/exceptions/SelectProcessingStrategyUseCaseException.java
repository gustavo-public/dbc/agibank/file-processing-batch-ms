package br.com.agibank.file_processing_batch_ms.service.exceptions;

public class SelectProcessingStrategyUseCaseException extends UseCaseException{
    public SelectProcessingStrategyUseCaseException(Throwable cause) {
        super("Error on select processing strategy", cause);
    }
}
