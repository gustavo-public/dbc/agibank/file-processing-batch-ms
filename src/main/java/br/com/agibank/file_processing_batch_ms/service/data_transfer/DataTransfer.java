package br.com.agibank.file_processing_batch_ms.service.data_transfer;

public interface DataTransfer <S,T>{

    T transfer(S source,T target);
}
