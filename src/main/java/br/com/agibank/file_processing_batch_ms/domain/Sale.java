package br.com.agibank.file_processing_batch_ms.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

@Builder
@Getter
@Setter
public class Sale {

    private String id;
    private String salesManName;

    private BigDecimal saleAmount;

    private List<SaleItem> items;

}
