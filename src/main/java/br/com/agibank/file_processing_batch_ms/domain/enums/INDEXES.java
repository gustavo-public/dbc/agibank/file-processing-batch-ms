package br.com.agibank.file_processing_batch_ms.domain.enums;

public enum INDEXES {

    CODE(0);

    public enum SALES_MAN{
        CPF(1),NAME(2),SALARY(3);
        private final int INDEX;

        private SALES_MAN(final int INDEX){
            this.INDEX = INDEX;
        }
        public int getIndex(){
            return this.INDEX;
        }
    }

    public enum SALE{
        ID(1),ITEMS(2),SALES_MAN(3);

        private final int INDEX;

        private SALE(final int INDEX){
            this.INDEX = INDEX;
        }

        public int getIndex(){
            return this.INDEX;
        }
    }

    public enum SALE_ITEM{
        ID(0),QUANTITY(1),PRICE(2);

        private final int INDEX;

        private SALE_ITEM(final int INDEX){
            this.INDEX = INDEX;
        }

        public int getIndex(){
            return this.INDEX;
        }
    }

    public enum  CLIENT{

        CPNJ(1),NAME(2),BUSINESS_AREA(3);

        private final int INDEX;

        private CLIENT(final int INDEX){
            this.INDEX = INDEX;
        }

        public int getIndex(){
            return this.INDEX;
        }
    }

    private final int INDEX;

    private INDEXES(final int INDEX){
        this.INDEX = INDEX;
    }

    public int getIndex(){
        return this.INDEX;
    }

}
