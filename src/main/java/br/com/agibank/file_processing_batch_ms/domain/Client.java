package br.com.agibank.file_processing_batch_ms.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class Client {

    private String cnpj;
    private String name;
    private String businessArea;

}
