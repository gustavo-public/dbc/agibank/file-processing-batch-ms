package br.com.agibank.file_processing_batch_ms.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Map;

@Builder
@Getter
@Setter
public class FileProcessData {

    private String fileName;
    private String fileContent;
    private String moreExpensiveSaleId;

    private Integer clientQuantity;
    private Integer salesManQuantity;

    private String[]lineData;

    private Sale expensiveSale;
    private Sale currentSale;

    private String worstSalesManName;
    private SalesMan currentSalesMan;

    private List<Client>clientList;
    private List<SalesMan>salesManList;

    private Map<String, BigDecimal> salesManSaleAmountMap;

    private LocalTime startTime;

}
