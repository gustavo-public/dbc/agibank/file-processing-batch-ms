package br.com.agibank.file_processing_batch_ms.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalTime;
import java.util.List;
import java.util.Map;

@Builder
@Getter
@Setter
public class FileProcessReport {

    private String fileName;

    private Integer clientQuantity;
    private Integer salesManQuantity;

    private String moreExpensiveSaleId;
    private String worstSalesManName;

    private LocalTime startTime;


}
