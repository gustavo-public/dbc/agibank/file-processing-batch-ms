package br.com.agibank.file_processing_batch_ms.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Builder
@Getter
@Setter
public class SaleItem {

    private String id;

    private BigDecimal quantity;
    private BigDecimal price;

}
