package br.com.agibank.file_processing_batch_ms.service.use_case

import br.com.agibank.file_processing_batch_ms.service.use_case.ReadFileUseCase
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest
class ReadFileUseCaseSpec extends Specification {

    @Autowired
    private ReadFileUseCase useCase;

    def "length of file content '#a' must be size #b"() {

        given: "A temporal file"
        def file = new File("temp_file.dat")

        when: "Reading data from file"
        file.write(a)
        def monoResult = useCase.execute(file.toPath())

        then: "Expected size is correct"
        monoResult.block().length() == b

        where:
        a               || b
        "test"          || 4
        "nome de alguem"|| 14
        "a"             || 1
        ""              || 0
    }

}
