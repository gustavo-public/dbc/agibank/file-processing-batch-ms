package br.com.agibank.file_processing_batch_ms.service.use_case.report

import br.com.agibank.file_processing_batch_ms.domain.Client
import br.com.agibank.file_processing_batch_ms.domain.FileProcessData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import spock.lang.Specification;

@SpringBootTest
public class CalculateClientQuantityUseCaseSpec extends Specification {

    @Autowired
    CalculateClientQuantityUseCase useCase;

    def "Should calculate client quantity correctly" (){

        given: "A new file process data with some clients"
        def processData = FileProcessData.builder()
                                        .clientList(List.of(Client.builder().build()))
                                        .build()

        when: "Calculate client quantity"
        useCase.execute(processData).block()

        then: "Client quantity must be updated"
        processData.getClientQuantity() == 1

    }

}
