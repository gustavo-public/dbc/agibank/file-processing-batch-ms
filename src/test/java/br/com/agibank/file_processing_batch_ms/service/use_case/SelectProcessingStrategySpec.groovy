package br.com.agibank.file_processing_batch_ms.service.use_case

import br.com.agibank.file_processing_batch_ms.service.exceptions.ProcessingStrategyCodeRequiredException
import br.com.agibank.file_processing_batch_ms.service.exceptions.ProcessingStrategyNotFoundException
import br.com.agibank.file_processing_batch_ms.service.exceptions.SelectProcessingStrategyUseCaseException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest
public class SelectProcessingStrategySpec extends Specification {

    @Autowired
    private SelectProcessingStrategyUseCase useCase;

    def "Should throw #expectedException.class.simpleName with cause #expectedCause.simpleName when wrong code #wrongCode is provided"() {
        given: "A wrong code"
        def code = wrongCode

        when: "Select a strategy by wrong code"
        useCase.execute(code).block()

        then: "Expected exception must be thrown"

        def error = thrown(expectedException)
        error.getClass() == SelectProcessingStrategyUseCaseException.class
        error.getCause() != null
        error.getCause().class.name == expectedCause.name

        where:
        wrongCode         | expectedException                        | expectedCause
        "01"              | SelectProcessingStrategyUseCaseException | ProcessingStrategyNotFoundException
        "1"               | SelectProcessingStrategyUseCaseException | ProcessingStrategyNotFoundException
        "008"             | SelectProcessingStrategyUseCaseException | ProcessingStrategyNotFoundException
        "some wrong code" | SelectProcessingStrategyUseCaseException | ProcessingStrategyNotFoundException
        ""                | SelectProcessingStrategyUseCaseException | ProcessingStrategyCodeRequiredException
        "       "         | SelectProcessingStrategyUseCaseException | ProcessingStrategyCodeRequiredException
        null              | SelectProcessingStrategyUseCaseException | ProcessingStrategyCodeRequiredException
    }

}
