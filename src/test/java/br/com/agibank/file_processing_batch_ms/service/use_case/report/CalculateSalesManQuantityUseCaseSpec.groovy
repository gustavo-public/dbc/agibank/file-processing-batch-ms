package br.com.agibank.file_processing_batch_ms.service.use_case.report

import br.com.agibank.file_processing_batch_ms.domain.Client
import br.com.agibank.file_processing_batch_ms.domain.FileProcessData
import br.com.agibank.file_processing_batch_ms.domain.SalesMan
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import spock.lang.Specification;

@SpringBootTest
public class CalculateSalesManQuantityUseCaseSpec extends Specification {

    @Autowired
    CalculateSalesManQuantityUseCase useCase;

    def "Should calculate salesman quantity correctly" (){

        given: "A new file process data with some salesmen"
        def processData = FileProcessData.builder()
                .salesManList(List.of(SalesMan.builder().build()))
                .build()

        when: "Calculate salesman quantity"
        useCase.execute(processData).block()

        then: "Salesman quantity must be updated"
        processData.getSalesManQuantity() == 1

    }
}
