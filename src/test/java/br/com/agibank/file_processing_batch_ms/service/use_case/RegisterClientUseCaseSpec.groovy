package br.com.agibank.file_processing_batch_ms.service.use_case

import br.com.agibank.file_processing_batch_ms.domain.Client
import br.com.agibank.file_processing_batch_ms.domain.FileProcessData
import br.com.agibank.file_processing_batch_ms.domain.SalesMan
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest
class RegisterClientUseCaseSpec extends Specification{

    @Autowired
    private RegisterClientUseCase useCase

    def separator = "ç"

    def "Should register client successfully" (){

        given: "A new client data"
        def clientData = "002ç2345675434544345çJose da SilvaçRural"
        def fileProcessData = FileProcessData.builder()
                                            .clientList(new LinkedList<Client>())
                                            .salesManList(new LinkedList<SalesMan>())
                                            .build()

        when: "Processing client data"
        fileProcessData.setLineData(clientData.split(separator))
        def result = useCase.execute(fileProcessData).block()

        then: "List size should be incremented"
        result.getClientList().size() == 1

    }

}
