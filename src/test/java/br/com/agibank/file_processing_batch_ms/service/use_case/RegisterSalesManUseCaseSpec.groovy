package br.com.agibank.file_processing_batch_ms.service.use_case

import br.com.agibank.file_processing_batch_ms.domain.Client
import br.com.agibank.file_processing_batch_ms.domain.FileProcessData
import br.com.agibank.file_processing_batch_ms.domain.SalesMan
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest
class RegisterSalesManUseCaseSpec extends  Specification{

    @Autowired
    RegisterSalesManUseCase useCase;

    def separator = "ç"

    def "Should register salesman correctly" (){

        given: "A salesman string data"
        def stringData = "001ç1234567891234çPedroç50000"
        def fileProcessData = FileProcessData.builder()
                .salesManSaleAmountMap(new HashMap<String, BigDecimal>())
                .clientList(new LinkedList<Client>())
                .salesManList(new LinkedList<SalesMan>())
                .build()

        when: "Process data"
        def saleData = stringData.split(separator)
        fileProcessData.setLineData(saleData)
        def result = useCase.execute(fileProcessData).block()

        then:
        result.getSalesManList().size() == 1

    }
}
