package br.com.agibank.file_processing_batch_ms.service.use_case

import br.com.agibank.file_processing_batch_ms.domain.FileProcessData
import br.com.agibank.file_processing_batch_ms.domain.Sale
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest
class CheckExpensiveSaleUseCaseSpec extends Specification {

    @Autowired
    private CheckExpensiveSaleUseCase useCase;

    def "Should define do expensive sale correctly"() {

        given: "A new sale to compare"
        def currentSale = Sale.builder()
                .saleAmount(new BigDecimal(currentSaleAmount))
                .salesManName(currentSaleSalesManName)
                .build()

        def currentExpensiveSale = Sale.builder()
                .saleAmount(new BigDecimal(currentExpensiveSaleAmount))
                .salesManName(currentExpensiveSaleSalesManName)
                .build()

        def fileProcessData = FileProcessData.builder()
                .currentSale(currentSale)
                .expensiveSale(currentExpensiveSale)
                .build();

        when: "Check if current sale is more expensive"
        def result = useCase.execute(fileProcessData).block()

        then:
        def expensiveSale = result.getExpensiveSale()
        expensiveSale.salesManName == expectedSaleSalesManName
        expensiveSale.saleAmount == new BigDecimal(expectedSaleAmount)

        where:
        currentSaleSalesManName | currentSaleAmount | currentExpensiveSaleSalesManName | currentExpensiveSaleAmount || expectedSaleSalesManName | expectedSaleAmount
        "s1"                    | "0.99"            | "s2"                             | "1.0"                      || "s2"                     | "1.0"
        "s1"                    | "1.0"             | "s2"                             | "0.99"                     || "s1"                     | "1.0"
        "s1"                    | "0.99"            | "s2"                             | "0.99"                     || "s2"                     | "0.99"

    }


    def "Should define new sale as expensive when has no expensive sale yet" (){

        given: "A new sale to compare"
        def sale = Sale.builder()
                        .salesManName("s1")
                        .saleAmount(new BigDecimal("0.1"))
                        .build()
        def fileProcessData = FileProcessData.builder()
                                            .currentSale(sale)
                                            .build()

        when: "Process data"
        def result = useCase.execute(fileProcessData).block()

        then:
        result.expensiveSale != null
        result.expensiveSale.saleAmount == new BigDecimal("0.1")
        result.expensiveSale.salesManName == "s1"

    }

}
