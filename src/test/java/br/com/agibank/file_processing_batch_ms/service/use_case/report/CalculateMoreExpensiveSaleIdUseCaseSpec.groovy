package br.com.agibank.file_processing_batch_ms.service.use_case.report


import br.com.agibank.file_processing_batch_ms.domain.FileProcessData
import br.com.agibank.file_processing_batch_ms.domain.Sale
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest
public class CalculateMoreExpensiveSaleIdUseCaseSpec extends Specification {

    @Autowired
    CalculateMoreExpensiveSaleIdUseCase useCase;

    def "Should provide the id of more expensive sale" (){

        given: "A process data"
        def processData = FileProcessData.builder()
                .expensiveSale(Sale.builder().id("ID_MORE_EXPENSIVE").build())
                .build()

        when: "Process data"
        useCase.execute(processData).block()

        then: "More expensive id must be updated"
        processData.getMoreExpensiveSaleId() == "ID_MORE_EXPENSIVE"

    }

}
