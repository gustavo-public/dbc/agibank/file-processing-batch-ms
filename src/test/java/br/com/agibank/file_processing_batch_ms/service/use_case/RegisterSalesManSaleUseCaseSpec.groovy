package br.com.agibank.file_processing_batch_ms.service.use_case

import br.com.agibank.file_processing_batch_ms.domain.FileProcessData
import br.com.agibank.file_processing_batch_ms.domain.Sale
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest
class RegisterSalesManSaleUseCaseSpec extends Specification {

    @Autowired
    RegisterSalesManSaleUseCase useCase

    def "Should increment sales man sale amount correctly"() {

        given: "A new sale for sales man"
        def sale = Sale.builder()
                .salesManName("s1")
                .saleAmount(new BigDecimal("0.1"))
                .build()

        def salesManMap = new HashMap()
        salesManMap.put("s1", new BigDecimal("0.9"))

        def fileProcessData = FileProcessData.builder()
                .currentSale(sale)
                .salesManSaleAmountMap(salesManMap)
                .build()

        when: "Process data"
        def result = useCase.execute(fileProcessData).block()

        then:
        result.getSalesManSaleAmountMap().get("s1") == new BigDecimal("1.0")

    }

    def "Should sum sale amount correctly for sale"() {

        given: "Sale data for sales man"
        def sales = saleData.stream()
                .map(val -> Sale.builder()
                        .salesManName(salesMan)
                        .saleAmount(new BigDecimal(val))
                        .build())

        when: "Process data"
        def fileProcessData = FileProcessData.builder()
                                            .salesManSaleAmountMap(new HashMap<String, BigDecimal>())
                                            .build()
        sales.forEach(s -> {
            fileProcessData.setCurrentSale(s)
            useCase.execute(fileProcessData).block()
        })


        then:
        fileProcessData.getSalesManSaleAmountMap().get(salesMan) == new BigDecimal(expectedSalesManAmount)

        where:
        salesMan | saleData                 || expectedSalesManAmount
        "s1"     | ["0.11", "0.27", "0.19"] || "0.57"
        "s2"     | ["0.33", "0.7", "0.21"]  || "1.24"
        "s3"     | ["0.01", "0.01", "0.01"] || "0.03"
        "s4"     | ["0.99", "0.99", "0.99"] || "2.97"

    }

}
