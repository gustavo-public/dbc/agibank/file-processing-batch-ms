package br.com.agibank.file_processing_batch_ms.service.use_case

import br.com.agibank.file_processing_batch_ms.domain.Client
import br.com.agibank.file_processing_batch_ms.domain.FileProcessData
import br.com.agibank.file_processing_batch_ms.domain.Sale
import br.com.agibank.file_processing_batch_ms.domain.SalesMan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification;

@SpringBootTest
public class RegisterSaleUseCaseSpec extends Specification{

    @Autowired
    private RegisterSaleUseCase useCase;

    def separator = "ç"

    def "Should register sale successfully"() {

        given: "A new sale string"
        def saleString = "003ç10ç[1-10-100,2-30-2.50,3-40-3.10]çPedro"
        def fileProcessData = FileProcessData.builder()
                .salesManSaleAmountMap(new HashMap<String, BigDecimal>())
                .clientList(new LinkedList<Client>())
                .salesManList(new LinkedList<SalesMan>())
                .build()

        when: "Process sale data"
        def saleData = saleString.split(separator)
        fileProcessData.setLineData(saleData)
        def result = useCase.execute(fileProcessData).block()

        then: "Sale list size must be incremented"
        def sale = result.getCurrentSale()

        sale != null
        sale.id == "10"
        sale.salesManName == "Pedro"

    }

}
