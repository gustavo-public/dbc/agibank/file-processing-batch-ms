package br.com.agibank.file_processing_batch_ms.service.use_case

import br.com.agibank.file_processing_batch_ms.domain.Sale
import br.com.agibank.file_processing_batch_ms.domain.SaleItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import spock.lang.Specification;

@SpringBootTest
public class CalculateSaleAmountUseCaseSpec extends Specification {

    @Autowired
    private CalculateSaleAmountUseCase useCase

    def "Sale amount should be #expectedSaleAmount with items [#items]"() {

        given: "Sale with list items"
        def sale = Sale.builder()
                .id(UUID.randomUUID().toString())
                .items(items)
                .salesManName("Test")
                .build()

        when: "Calculate the sale amount"
        def saleCalculated = useCase.execute(sale).block();

        then: "Sale amount should be as expected"
        expectedSaleAmount == saleCalculated.getSaleAmount()

        where:
        items || expectedSaleAmount
        [
                SaleItem.builder().id('1').price(new BigDecimal("2")).quantity(new BigDecimal("2")).build()
        ]     || new BigDecimal("4")
        [
                SaleItem.builder().id('1').price(new BigDecimal("2")).quantity(new BigDecimal("2")).build(),
                SaleItem.builder().id('2').price(new BigDecimal("2.5")).quantity(new BigDecimal("2")).build()
        ]     || new BigDecimal("9")
        [
                SaleItem.builder().id('1').price(new BigDecimal("1.35")).quantity(new BigDecimal("3")).build(),
                SaleItem.builder().id('2').price(new BigDecimal("13.57")).quantity(new BigDecimal("7")).build(),
                SaleItem.builder().id('2').price(new BigDecimal("3.33")).quantity(new BigDecimal("11")).build()
        ]     || new BigDecimal("135.67")
        [
                SaleItem.builder().id('1').price(new BigDecimal("1.39")).quantity(new BigDecimal("23")).build(),
                SaleItem.builder().id('2').price(new BigDecimal("11.57")).quantity(new BigDecimal("71")).build(),
                SaleItem.builder().id('3').price(new BigDecimal("51.33")).quantity(new BigDecimal("11")).build(),
                SaleItem.builder().id('4').price(new BigDecimal("27.19")).quantity(new BigDecimal("17")).build(),
                SaleItem.builder().id('5').price(new BigDecimal("13.77")).quantity(new BigDecimal("19")).build(),
        ]     || new BigDecimal("2141.93")
    }


}
