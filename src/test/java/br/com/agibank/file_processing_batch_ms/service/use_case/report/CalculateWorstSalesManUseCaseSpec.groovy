package br.com.agibank.file_processing_batch_ms.service.use_case.report

import br.com.agibank.file_processing_batch_ms.domain.FileProcessData
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import spock.lang.Specification;

@SpringBootTest
public class CalculateWorstSalesManUseCaseSpec extends Specification {

    @Autowired
    CalculateWorstSalesManUseCase useCase

    def "Must find the worst salesman" (){

        given: "Data from several salesmen"
        def processData = FileProcessData.builder()
                                        .salesManSaleAmountMap(Map.of(
                                                "BETTER_SALES_MAN", new BigDecimal("100.00"),
                                                "SALES_MAN_1", new BigDecimal("50.00"),
                                                "SALES_MAN_2", new BigDecimal("35.00"),
                                                "WORST_SALESMAN", new BigDecimal("0.01"),
                                                "SALES_MAN_3", new BigDecimal("10.00"),
                                        ))
                                        .build()
        when: "Process data"
        useCase.execute(processData).block()

        then: "Worst salesman must be updated"
        processData.getWorstSalesManName() == "WORST_SALESMAN"

    }

}
