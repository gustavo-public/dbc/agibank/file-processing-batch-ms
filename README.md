# Projeto: file-processing-batch-ms

Projeto para processamento de arquivos do banco agibank

# O desafio
Criar um sistema de alta performance para processamento de arquivos

## Tecnologias utilizadas

### na implementação
- Spring boot
- Spring WebFlux
- Spring batch
- Spring devtools - FileWatcher
- Lombok
- MapStruct
- Clean Architecture
- JDK 15 (Text Block)

### nos testes unitários
- TDD
- BDD
- Spock framework
- Groovy

### no versionamento de código
- Git Lab

### no deploy
- docker 
- docker-compose 
- Gitlab container registry

## Execução do projeto

- Necessário instalação do docker:
    - [Instruções de instalação no ubuntu aqui](https://docs.docker.com/engine/install/ubuntu/)
    - [Instruções de instalação no windows aqui](https://docs.docker.com/docker-for-windows/install/)
- Necessário instalação do docker-compose:
    - [Instruções para instalação linux e windows aqui](https://docs.docker.com/compose/install/)

- Após clonar o projeto, basta entrar no diretorio raiz do projeto e executar o comando:
    - **docker-compose up** - caso queria executar e acompanhar os logs da aplicação
    - **docker-compose up -d** - para executar em *detached mode* para executar o projeto em background
  

em ambas opções o docker irá realizar o pull da docker image do projeto e executar a aplicação a partir da imagem
