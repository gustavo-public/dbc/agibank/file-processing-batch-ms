FROM gradle:6.7-jdk15 as Builder

COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle build --no-daemon

FROM mcr.microsoft.com/java/jre:15-zulu-alpine

COPY --from=builder /home/gradle/src/build/libs/*.jar ./app.jar

RUN mkdir -p ~/data/in && mkdir ~/data/out && chmod 777 -R ~/data

ENTRYPOINT ["java","-jar","./app.jar"]
